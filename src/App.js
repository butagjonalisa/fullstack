
import './App.css';
import AppNavbar from './components/AppNavbar'
import Container from 'react-bootstrap/Container'

import Home from './pages/Home'
import Courses from './pages/Courses'
import Register from './pages/Register'
import Login from './pages/Login'


function App() {

  return (
    // React Fragments <></>
    <>
      <AppNavbar />
      <Container>
        <Register/>
        <Login/>
      </Container>
    </> 
  );
}

export default App;
