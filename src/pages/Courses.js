import coursesData from '../data/courseData';
import CourseCard from '../components/CourseCard'

export default function Courses() {

	// console.log(coursesData);
	// console.log(coursesData[0])

	const courses = coursesData.map(course => {
		return (
			<CourseCard key={course.id} courseProp={course} />
		)
	})

	return (
		<>
			{courses}
		</>
	)
}


